from fastapi import (
    APIRouter,
    Depends,
    Response,
    Request,
    HTTPException,
)
from queries.users import (
    UserIn,
    UserInNoPassword,
    UserOut,
    UserRepository,
)
import authenticator


router = APIRouter()


@router.post("/api/users", response_model=UserOut)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    user_repository: UserRepository = Depends(),
):
    try:
        user.password = authenticator.hash_password(user.password)
        user = user_repository.create(user)
        response.status_code = 201
        return user
    except Exception as e:
        raise HTTPException(
            status_code=400, detail=str(e))


@router.get("/api/users", response_model=dict)
def get_all_users(
    repo: UserRepository = Depends(),
):
    users = repo.get_all_users()
    return {
        "users": users
    }


@router.get("/api/users/{user_id}", response_model=UserOut)
def get_user(
    user_id: int,
    repo: UserRepository = Depends(),
):
    user = repo.get(user_id)
    if user is None:
        raise HTTPException(
            status_code=404, detail="User not found")
    return user


@router.delete("/api/users/{user_id}", response_model=dict)
def delete_user(
    user_id: int,
    repo: UserRepository = Depends(),
):
    user = repo.delete(user_id)
    if user is False:
        raise HTTPException(
            status_code=404, detail="User not found")
    return {"message": "User deleted"}


@router.put("/api/users/{user_id}", response_model=UserOut)
def put_user(
    user_id: int,
    user: UserInNoPassword,
    repo: UserRepository = Depends(),
):
    user = repo.update(user_id, user)
    if user is None:
        raise HTTPException(
            status_code=404, detail="User not found")
    return user
