from typing import List
from fastapi import APIRouter, Depends
from queries.prompts import PromptIn, PromptRepository, PromptOut


router = APIRouter()


@router.post('/api/prompts/', response_model=PromptOut)
def create_prompt(
    prompt: PromptIn,
    repo: PromptRepository = Depends(),
):
    return repo.create(prompt)


@router.get('/api/prompts/{prompt_id}', response_model=PromptOut)
def get_one_prompt(
    prompt_id: int,
    repo: PromptRepository = Depends(),
):
    return repo.get_prompt_by_id(prompt_id)


@router.put('/api/prompts/{prompt_id}', response_model=PromptOut)
def update_prompt(
    prompt_id: int,
    prompt: PromptIn,
    repo: PromptRepository = Depends(),
):
    return repo.update(prompt_id, prompt)


@router.delete('/api/prompts/{prompt_id}', response_model=bool)
def delete_prompt(
    prompt_id: int,
    repo: PromptRepository = Depends(),
):
    return repo.delete(prompt_id)
