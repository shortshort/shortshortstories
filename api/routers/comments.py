from typing import List
from fastapi import APIRouter, Depends
from queries.comments import CommentIn, CommentRepository, CommentOut

router = APIRouter()


@router.post('/api/submissions/{submission_id}/comments/', response_model=CommentOut)
def create_comment(
    comment: CommentIn,
    repo: CommentRepository = Depends(),
):
    return repo.create(comment)


@router.get('/api/{user_id}/comments/', response_model=List[CommentOut])
def get_all_comments_by_user(
    user_id: int,
    repo: CommentRepository = Depends(),
):
    return repo.get_comments_by_user(user_id)


@router.get('/api/submissions/{submission_id}/comments/', response_model=List[CommentOut])
def get_all_comments_by_submission(
    submission_id: int,
    repo: CommentRepository = Depends(),
):
    return repo.get_comments_by_submission(submission_id)


@router.put('/api/submissions/{submission_id}/comments/{comment_id}', response_model=CommentOut)
def update_comment(
    comment_id: int,
    comment: CommentIn,
    repo: CommentRepository = Depends(),
):
    return repo.update(comment_id, comment)


@router.delete('/api/submissions/{submission_id}/comments/{comment_id}', response_model=bool)
def delete_comment(
    comment_id: int,
    repo: CommentRepository = Depends(),
):
    return repo.delete(comment_id)
