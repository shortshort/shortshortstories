from pydantic import BaseModel
from typing import List, Optional
from datetime import date
from queries.pool import pool
from fastapi import HTTPException


class PromptIn(BaseModel):
    content: str


class PromptOut(BaseModel):
    id: int
    content: str


class PromptRepository:
    def create(self, prompt: PromptIn) -> PromptOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO prompt
                        (content)
                    VALUES
                        (%s)
                    RETURNING id;
                    """,
                    [
                        prompt.content
                    ]
                )
                id = result.fetchone()[0]
                old_data = prompt.dict()
                return PromptOut(id=id, **old_data)


    def get_prompt_by_id(self, prompt_id: int) -> PromptOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, content
                        FROM prompt
                        WHERE id = %s;
                        """,
                        [prompt_id]
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(status_code=404, detail="Prompt not found")
                    return PromptOut(id=result[0], content=result[1])
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))


    def update(self, prompt_id: int, prompt: PromptIn) -> PromptOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE prompt
                        SET content = %s
                        WHERE id = %s;
                        """,
                        [prompt.content, prompt_id]
                    )
                    if db.rowcount == 0:
                        raise HTTPException(status_code=404, detail="Prompt not found")
                    return PromptOut(id=prompt_id, **prompt.dict())
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))


    def delete(self, prompt_id: int) -> None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM prompt
                        WHERE id = %s;
                        """,
                        [prompt_id]
                    )
                    if db.rowcount == 0:
                        raise HTTPException(status_code=404, detail="Prompt not found")
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))
