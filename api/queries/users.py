from pydantic import BaseModel
from typing import Optional, List
from queries.pool import pool


class UserIn(BaseModel):
    username: str
    email: str
    password: str
    bio: Optional[str]
    avatar_picture: Optional[str]


class UserInNoPassword(BaseModel):
    username: str
    email: str
    bio: Optional[str]
    avatar_picture: Optional[str]


class UserOut(BaseModel):
    id: int
    username: str
    email: str
    bio: Optional[str]
    avatar_picture: Optional[str]


class UserOutWithPassword(UserOut):
    password: str


class UserRepository:
    def create(
            self, user: UserIn
    ) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users (
                        username,
                        email,
                        password,
                        bio,
                        avatar_picture)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING
                        id,
                        username,
                        email,
                        bio,
                        avatar_picture
                    """,
                    [
                        user.username,
                        user.email,
                        user.password,
                        user.bio,
                        user.avatar_picture,
                    ],
                )
                id = result.fetchone()[0]
                new_user = UserOut(id=id, **user.dict())
                return new_user

    def get_all_users(self) -> List[UserOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    email,
                    bio,
                    avatar_picture
                    FROM users
                    ORDER BY username
                    """
                )
                result = db.fetchall()
                users = []
                for record in result:
                    user = UserOut(
                        id=record[0],
                        username=record[1],
                        email=record[2],
                        bio=record[3],
                        avatar_picture=record[4],
                    )
                    users.append(user)
                return users

    def get(self, user_id: int) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    email,
                    bio,
                    avatar_picture
                    FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )
                record = db.fetchone()
                if record is None:
                    return None
                user = UserOut(
                    id=record[0],
                    username=record[1],
                    email=record[2],
                    bio=record[4],
                    avatar_picture=record[5],
                )
                return user

    def get_username(self, username: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    email,
                    password,
                    bio,
                    avatar_picture
                    FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                record = db.fetchone()
                if record is None:
                    return None
                user = UserOutWithPassword(
                    id=record[0],
                    username=record[1],
                    email=record[2],
                    password=record[3],
                    bio=record[4],
                    avatar_picture=record[5],
                )
                return user

    def delete(self, user_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )
                return bool(result.rowcount)

    def update(self, user_id: int, user: UserInNoPassword) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE users
                    SET
                        username = %s,
                        email = %s,
                        bio = %s,
                        avatar_picture = %s
                    WHERE id = %s
                    RETURNING
                        id,
                        username,
                        email,
                        bio,
                        avatar_picture
                    """,
                    [
                        user.username,
                        user.email,
                        user.bio,
                        user.avatar_picture,
                        user_id,
                    ],
                )
                record = result.fetchone()
                if record is None:
                    return None
                user = UserOut(
                    id=record[0],
                    username=record[1],
                    email=record[2],
                    bio=record[3],
                    avatar_picture=record[4],
                )
                return user
