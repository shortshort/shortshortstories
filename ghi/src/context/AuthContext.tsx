import { createContext, useContext, ReactNode, useState } from 'react';

type AuthContextType = {
    token: User | null;
    login: (user: User) => void;
    logout: () => void;
}

type User = {
    id: number;
    username: string;
    exp: number;
}
export default User;
const AuthContext = createContext<AuthContextType>({
    token: null,
    login: () => { },
    logout: () => { }
});

type AuthProviderProps = {
    children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
    const [token, setToken] = useState<User | null>(null);

    const login = (user: User) => {
        setToken(token);
    }

    const logout = () => {
        setToken(null);
    }

    return (
        <AuthContext.Provider value={{ token, login, logout }}>
            {children}
        </AuthContext.Provider>
    )
}

export const useAuth = (): AuthContextType => {
    const context = useContext(AuthContext);
    if (context === undefined) {
        throw new Error('useAuth must be used within a AuthProvider')
    }
    return context;
}
