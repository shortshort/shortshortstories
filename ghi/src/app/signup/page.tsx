'use client';

import { Button, Label, TextInput } from 'flowbite-react';
import { useRouter } from 'next/navigation';
import { useState } from 'react';

const defaultFormData = {
    username: '',
    email: '',
    password: '',
    bio: 'Placeholder bio',
    avatar_picture: ''
}

export default function SignupPage() {
    const [formData, setFormData] = useState(defaultFormData);
    const router = useRouter();

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData((prev) => ({
            ...prev,
            [e.target.id]: e.target.value
        }));
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const url = `${process.env.NEXT_PUBLIC_REACT_APP_API_HOST}/api/users`;
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData(defaultFormData);
            router.push('/');
        } else {
            const errorData = await response.json()
            throw new Error(errorData.message || "Failed to create user");
        }
    }

    return (
        <form onSubmit={handleSubmit} className="flex max-w-md flex-col gap-4">
            <div>
                <div className="mb-2 block">
                    <Label htmlFor="username" value="Username" />
                </div>
                <TextInput onChange={handleChange}
                id="username" type="text"
                placeholder="type username here" required />
            </div>
            <div>
                <div className="mb-2 block">
                    <Label htmlFor="email" value="Email" />
                </div>
                <TextInput onChange={handleChange} id="email"
                type="email" placeholder="name@email.com"
                required />
            </div>
            <div>
                <div className="mb-2 block">
                    <Label htmlFor="password" value="Password" />
                </div>
                <TextInput onChange={handleChange} id="password"
                type="password" required />
            </div>
            <Button type="submit">Submit</Button>
        </form>
    );
}
