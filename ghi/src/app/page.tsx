'use client'

import NavBar from './navbar';
import React, { useEffect, useState } from 'react';
export default function MainPage() {

  interface Prompt {
    id: number;
    content: string;
  }

  let count = 1;

  function Count() {
    count += 1;
  }

  setInterval(Count, 1000 * 60 * 60 * 24);

  const [prompt, setPrompt] = useState<Prompt>({ id: 0, content: ""});

  // Prompts

  async function fetchPrompt() {
    const url = `${process.env.NEXT_PUBLIC_REACT_APP_API_HOST}/api/prompts/${count}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setPrompt(data);
      console.log(prompt)
    }
  }

  useEffect(() => {
    fetchPrompt();
  }, []);














    return(
      <>
        <NavBar />
        {prompt ? (
  <h1>{prompt.content}</h1>
) : (
  <p>Loading...</p>
)}
        </>
    )
}
