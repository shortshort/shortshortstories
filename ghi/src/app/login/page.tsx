'use client';

import { Button, Checkbox, Label, TextInput } from 'flowbite-react';
import { useRouter } from 'next/navigation';
import User, { useAuth } from '@/context/AuthContext'
import { useState } from 'react';
import jwt from 'jsonwebtoken'

const defaultFormData = {
    username: '',
    password: ''
}

export default function Login() {
    const [formData, setFormData] = useState(defaultFormData);
    const router = useRouter();
    const { login } = useAuth();
    const signingKey = process.env.NEXT_PUBLIC_SIGNING_KEY|| 'secret';

    function decodeToken(token: string, key: string) {
        try {
            const decodedToken = jwt.decode(token) as User;
            login(decodedToken);
        } catch (error) {
            console.log("Error receiving token: ", error);
        }
    }

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData((prev) => ({
            ...prev,
            [e.target.id]: e.target.value
        }));
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const loginData = new URLSearchParams();
        loginData.append('username', formData.username);
        loginData.append('password', formData.password);
        const url = `${process.env.NEXT_PUBLIC_REACT_APP_API_HOST}/token`;
        const fetchConfig = {
            method: 'POST',
            body: loginData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            decodeToken(data.access_token, signingKey)
            setFormData(defaultFormData);
            router.push('/');
        } else {
            const errorData = await response.json()
            console.log("error", errorData)
        }
    }

    return (
        <form onSubmit={handleSubmit} className="flex max-w-md flex-col gap-4">
            <div>
                <div className="mb-2 block">
                    <Label htmlFor="username" value="username" />
                </div>
                <TextInput onChange={handleChange} id="username"
                type="text" placeholder="Your username" required />
            </div>
        <div>
            <div className="mb-2 block">
                <Label htmlFor="password" value="password" />
            </div>
            <TextInput onChange={handleChange} id="password"
            type="password" required />
        </div>
        <Button type="submit">Submit</Button>
        </form>
    );
}
